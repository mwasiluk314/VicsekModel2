//author: Anita Zagrobelna
package pl.edu.pw.fizyka.pojava.AZMW;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.imageio.ImageIO;
import javax.swing.JPanel;

import java.awt.Color;
import java.io.File;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.SwingUtilities;



public class BoardPanel extends JPanel {
	

	

	private static final long serialVersionUID = 1L;

	
	public void setSecondBoard(Boolean arg0) {
		secondBoard=arg0;
	}
	
	public Boolean isSecondBoard(){
		return secondBoard;
	}
	
	void addParticle(){
		Random rand = new Random();
		Particle pr = new Particle(rand, this);
		particles.add(pr);
		this.repaint();
	}
	
	
	
	public void paintComponent(Graphics g) {
		g.setColor(Color.white);
		g.fillRect(0, 0,this.getWidth(),this.getHeight());
		g.setColor(Color.black);
		if(secondBoard==false) {
			g.drawOval(xPos, yPos, rInt, rInt);
	        g.drawOval(xPos-((rExt-rInt)/2), yPos-((rExt-rInt)/2), rExt, rExt);
		}
		else {
			g.drawOval(xPos+(rInt2/4), yPos-rInt2/2, rInt2, rInt2);
			g.drawOval(xPos+rInt2/4, yPos+rInt2, rInt2, rInt2);
			g.drawOval(xPos-((rExt-rInt)/2), yPos-((rExt-rInt)/2), rExt, rExt);
		}
        
        for(Particle pr : particles) {
        	pr.paint(g);
        }
        
    }
	
	public int getBoardXPos(){
		return xPos;
	}
	
	public int getBoardYPos(){
		return yPos;
	}
	
	public int getBoardRInt(){
		return rInt;
	}
	
	public int getBoardRInt2(){
		return rInt2;
	}
	
	public int getBoardRExt(){
		return rExt;
	}
	
	public void setParticles (List<Particle> par) {
		this.particles = par;
	}
	
	List<Particle> particles = new ArrayList<Particle>();
	private Boolean secondBoard = false;
	private static int xPos=150;
	private static int yPos=100;
	private static int rInt=150;
	private static int rInt2=100;
	private static int rExt=300;
	
}
	
