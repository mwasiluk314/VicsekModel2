//author: Mateusz Wasiluk
package pl.edu.pw.fizyka.pojava.AZMW;

import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class ParticleNumberListener  implements ChangeListener{

	ParticleNumberListener(JSlider slider, JTextField txtField,BoardPanel board) {
		this.slider=slider;
		this.txtField=txtField;
		this.board=board;
	}

	@Override
	public void stateChanged(ChangeEvent arg0) {
	

		if(board.particles == null){
			board.addParticle();
		}
		while(slider.getValue()>board.particles.size()){
			board.addParticle();
			board.repaint();
		}
		while(slider.getValue()<board.particles.size()){
			board.particles.remove(board.particles.size()-1);
			board.repaint();
		}
		String value = String.format("%d", slider.getValue());
		txtField.setText(value);
		

	}
	
	JSlider slider;
	JTextField txtField;
	BoardPanel board;
}
