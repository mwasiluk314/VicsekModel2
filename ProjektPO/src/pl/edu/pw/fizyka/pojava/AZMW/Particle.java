//author: Mateusz Wasiluk
package pl.edu.pw.fizyka.pojava.AZMW;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;




public class Particle {

	
	public Particle(Random r,BoardPanel board){
		int dist = 0;
		double phi = 0;
		Boolean freeSpace = false;
		do{
			phi = r.nextInt(359);
			phi = Math.toRadians(phi);
			if(board.isSecondBoard()==false)dist = board.getBoardRInt()/2+(int)((board.getBoardRExt()/2-board.getBoardRInt()/2)*r.nextDouble());
			else dist = (int)((board.getBoardRExt()/2)*r.nextDouble());
			xPos = board.getBoardXPos()+board.getBoardRInt()/2+(int)(dist*Math.cos(phi));
			yPos = board.getBoardYPos()+board.getBoardRInt()/2+(int)(dist*Math.sin(phi));
			if(board.isSecondBoard()==false) freeSpace=true;
			else if(
					Math.hypot(xPos-(board.getBoardXPos()+(board.getBoardRInt2()/4)+board.getBoardRInt2()/2), yPos-board.getBoardYPos())>board.getBoardRInt2()/2
					&&
					Math.hypot(xPos-(board.getBoardXPos()+(board.getBoardRInt2()/4)+board.getBoardRInt2()/2), yPos-board.getBoardYPos()-1.5*board.getBoardRInt2())>board.getBoardRInt2()/2
					) freeSpace=true;
		}while(freeSpace==false);
		
		this.setDirection(Math.toRadians(r.nextInt(359)));
		
	}
	
	public int getXPos(){ 
		return xPos;
		}
	
	public int getYPos(){
		return yPos;
	}
	
	public double getXDir(){
		return xDir;
	}
	
	public double getYDir(){
		return yDir;
	}
	
	public double getTheta(){
		return theta;
	}
	
	public static Color getColor(){
		return color;
	}
	
	public void setXPos(int xPos){
		this.xPos = xPos;
	}
	
	public void setYPos(int yPos){
		this.yPos = yPos;
	}
	
	public void moveParticle(int velocity){
		setXPos((int)Math.round(getXPos()+(velocity*getXDir())));
		setYPos((int)Math.round(getYPos()+(velocity*getYDir())));
	}
	

	public void setDirection(double thetaRad){
		xDir = Math.cos((thetaRad));
		yDir = Math.sin((thetaRad));
		theta = thetaRad;
	}
	
	public static void setColor(Color color){
		Particle.color = color;
	}
	public void paint(Graphics g){
        g.setColor(getColor());
        g.fillOval(xPos-3,yPos-3,5,5);
    }
	

	private int xPos;
	private int yPos;
	private double xDir;
	private double yDir;
	private double theta;
	static private Color color=Color.black;
}
