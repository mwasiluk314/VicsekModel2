//author: Anita Zagrobelna
package pl.edu.pw.fizyka.pojava.AZMW;

public class RunnableFrame implements Runnable {
	
	RunnableFrame(){
		mf = new MainFrame();
	}
	
	@Override
	public void run() {
		mf = new MainFrame();
		mf.setVisible(true);		
		}
		
	public MainFrame getMainFrame(){
		return mf;
	}		
	
	private MainFrame mf;
	

}
