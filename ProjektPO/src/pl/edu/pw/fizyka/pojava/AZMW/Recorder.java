//author: Mateusz Wasiluk
package pl.edu.pw.fizyka.pojava.AZMW;

import java.util.ArrayList;
import java.util.List;

public class Recorder {
	
	//Grupa odpowiada jednej "klatce flmowej"
	
	
	public ArrayList<Integer> getXPosGroup(int index){
		return xPosList.get(index);
	}
	
	public ArrayList<Integer> getYPosGroup(int index){
		return yPosList.get(index);
	}
	
	public Integer getXPos(int groupIndex, int particleIndex){
		return xPosList.get(groupIndex).get(particleIndex);
	}
	
	public Integer getYPos(int groupIndex, int particleIndex){
		return yPosList.get(groupIndex).get(particleIndex);
	}
	
	public void addGroup(){
		xPosList.add(new ArrayList<Integer>());
		yPosList.add(new ArrayList<Integer>());
	}
	
	public void addXPos(int groupIndex, int x){
		xPosList.get(groupIndex).add(x);
	}
	
	public void addYPos(int groupIndex, int y){
		yPosList.get(groupIndex).add(y);
	}
	
	private List<ArrayList<Integer>> xPosList = new ArrayList<ArrayList<Integer>>();
	private List<ArrayList<Integer>> yPosList = new ArrayList<ArrayList<Integer>>();
}