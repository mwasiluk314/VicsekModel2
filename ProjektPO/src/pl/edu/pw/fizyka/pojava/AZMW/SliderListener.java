//author: Mateusz Wasiluk
package pl.edu.pw.fizyka.pojava.AZMW;

import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class SliderListener implements ChangeListener {

	

		SliderListener(JSlider slider,JTextField txtField){
		this.txtField = txtField;
		this.slider = slider;

	}
	@Override
	public void stateChanged(ChangeEvent arg0) {
	
		String value = String.format("%d", slider.getValue());
		txtField.setText(value);
		

	}

	
	JTextField txtField;
	JSlider slider;


}

