//author: Anita Zagrobelna
package pl.edu.pw.fizyka.pojava.AZMW;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.HeadlessException;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Locale;
import java.util.ResourceBundle;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JColorChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.KeyStroke;



public class MainFrame extends JFrame {
	
	
	 Locale currentLocale;
	 ResourceBundle messages;
	 
	String string1="pl";
	String string2="PL";

	public MainFrame() throws HeadlessException {
		
		
		super("Vicsek Model");
		
	    
		
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setSize(820,500);
		this.setResizable(false);
		
		
		
	currentLocale = new Locale(string1, string2);
		Locale engLocale = new Locale("en","ENG");
		Locale  plLocale=new Locale("pl","PL");
		mainPanel = new JPanel(new BorderLayout());
		
		
		centerPanel1 = new JPanel();
		centerPanel1.setLayout(new BorderLayout());
		centerPanel2 = new BoardPanel();
		centerPanel2.setPreferredSize(new Dimension(474,362));
		centerPanel3 = new JPanel(new GridLayout(1,2));
		centerPanel3l = new JPanel(new BorderLayout());
		centerPanel3p = new JPanel(new BorderLayout());
		
	
		bottomPanel = new JPanel(new BorderLayout());
	    bottomPanel1 = new JPanel(new FlowLayout());
		bottomPanel2 = new JPanel(new GridLayout(1,2));
		bottomPanel2L = new JPanel();
		bottomPanel2L.setLayout(new BoxLayout(bottomPanel2L,BoxLayout.Y_AXIS));
		bottomPanel2R = new JPanel();
		bottomPanel2R.setLayout(new BoxLayout(bottomPanel2R,BoxLayout.Y_AXIS));
		bottomPanel2Ltop = new JPanel(new GridLayout(1,2));
		bottomPanel2Rtop = new JPanel(new GridLayout(1,2));
		bottomPanel3 = new JPanel();
		
	
		messages = ResourceBundle.getBundle("pl.edu.pw.fizyka.pojava.AZMW/MessagesBundle", currentLocale);
		
		//menu	
		
		
		
				menuBar = new JMenuBar();
				
				plik = new JMenu("Menu");
				submenu = new JMenu(messages.getString("submenu"));			
				submenu2 = new JMenuItem(messages.getString("submenu2"));
				
				submenu2.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						Color recievedColor = JColorChooser.showDialog(null, "Choose marker color", Particle.getColor());
		            	if(recievedColor != null) {
		            		Particle.setColor(recievedColor);
		            		centerPanel2.repaint();
		            		
		            		
		            	}
					}
				});
				
				
				menuItem = new JMenuItem(	"Polski");
				
				menuItem.addActionListener(	new ActionListener(){
					
					public void	 actionPerformed(ActionEvent arg0) {
				
						messages = ResourceBundle.getBundle("pl.edu.pw.fizyka.pojava.AZMW/MessagesBundle", plLocale);
						labelN.setText(messages.getString("labelN"));
						labelV.setText(messages.getString("labelV"));
						labelR.setText(messages.getString("labelR"));
						labelA.setText(messages.getString("labelA"));
						submenu.setText(messages.getString("submenu"));
						exit.setText(messages.getString("exit"));
						forward.setText(messages.getString("forward"));
						backwards.setText(messages.getString("backwards"));
						submenu2.setText(messages.getString("submenu2"));
						error1.setText(messages.getString("error1"));
						error2.setText(messages.getString("error2"));
						}});
					
				
				menuItem2 = new JMenuItem("English");
				
				menuItem2.addActionListener(	new ActionListener(){
					
					
					public void	 actionPerformed(ActionEvent arg0) {
					
					messages = ResourceBundle.getBundle("pl.edu.pw.fizyka.pojava.AZMW/MessagesBundle", engLocale);
					labelN.setText(messages.getString("labelN"));
					labelV.setText(messages.getString("labelV"));
					labelR.setText(messages.getString("labelR"));
					labelA.setText(messages.getString("labelA"));
					submenu.setText(messages.getString("submenu"));
					exit.setText(messages.getString("exit"));
					forward.setText(messages.getString("forward"));
					backwards.setText(messages.getString("backwards"));
					submenu2.setText(messages.getString("submenu2"));
					error1.setText(messages.getString("error1"));
					error2.setText(messages.getString("error2"));
						}});
				
				
				submenu.add(menuItem);
				submenu.add(menuItem2);
				plik.add(submenu);
				plik.add(submenu2);
				plik.addSeparator();
				
				exit = new JMenuItem(messages.getString("exit"));
				
				exit.addActionListener(	new ActionListener(){
					
					public void	 actionPerformed(ActionEvent arg0) {
						System.	exit(0);
						}});
					plik.add(exit);
					menuBar.add(plik);
					this.setJMenuBar(menuBar);
		
		
		
		//CenterPanel1
		
		
		board1 = new BoardButton ("circleBoard.gif",centerPanel2,false);
		board1.setPreferredSize(new Dimension(100,100));
		centerPanel1.add(board1,BorderLayout.PAGE_START);
		board2 = new BoardButton ("snowmanBoard.gif",centerPanel2,true);
		board2.setPreferredSize(new Dimension(100,100));
		centerPanel1.add(board2,BorderLayout.PAGE_END);
		
		
		//CenterPanel2
		
		centerPanel2.setBackground(Color.WHITE);
		
		
		//CenterPanel3l
		
		labelN = new JLabel(messages.getString("labelN"));
		centerPanel3l.add(labelN, BorderLayout.PAGE_START);
		
		final int SLIDER_MIN1 = 0;
		final int SLIDER_MAX1 = 2000;
		final int SLIDER_INIT1 = 0;
		
		sliderL = new JSlider(JSlider.VERTICAL, SLIDER_MIN1, SLIDER_MAX1, SLIDER_INIT1);
		fieldN=new JTextField("0");
		sliderL.addChangeListener(new ParticleNumberListener(sliderL,fieldN,centerPanel2));
		fieldN.addActionListener(new TxtFieldListener(sliderL,fieldN));
		centerPanel3l.add(sliderL, BorderLayout.CENTER);
		centerPanel3l.add(fieldN, BorderLayout.PAGE_END);
		
		
		//CenterPanel3p
		
		
		labelA = new JLabel(messages.getString("labelA"));
		centerPanel3p.add(labelA, BorderLayout.PAGE_START);
		
		final int SLIDER_MIN2 = 0;
		final int SLIDER_MAX2 = 100;
		final int SLIDER_INIT2 = 0;
		
		
		sliderA = new JSlider(JSlider.VERTICAL, SLIDER_MIN2, SLIDER_MAX2, SLIDER_INIT2);
		fieldA=new JTextField("0");
		sliderA.addChangeListener(new SliderListener(sliderA,fieldA));
		fieldA.addActionListener(new TxtFieldListener(sliderA,fieldA));
		centerPanel3p.add(sliderA, BorderLayout.CENTER);
		centerPanel3p.add(fieldA, BorderLayout.PAGE_END);
		
		
	
		//BottomPanel1
		
		play = new JButton ("Start");
		play.addActionListener(new ActionListener() {
			
		
			public void actionPerformed(ActionEvent e) {
				MotionCalculator.isWorking = true;
				sliderL.setEnabled(false);
				clipIndex=2;
			}
		});

		bottomPanel1.add(play, BorderLayout.PAGE_START);
		
		stop = new JButton ("Stop");
		stop.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				MotionCalculator.isWorking = false;
				sliderL.setEnabled(true);
			}
		});
		bottomPanel1.add(stop, BorderLayout.PAGE_START);
		
		error1= new JLabel(messages.getString("error1"));
		backwards = new JButton (messages.getString("backwards"));
		backwards.addActionListener(	new ActionListener(){
			
			public void	 actionPerformed(ActionEvent arg0) {
				int ii=0;
				if(clipIndex>MotionCalculator.groupCounter) {
					JOptionPane.showMessageDialog(null, error1);
					clipIndex--;
				}
				for(Particle par : centerPanel2.particles) {
					
					par.setXPos(MotionCalculator.rec.getXPos(MotionCalculator.groupCounter-clipIndex,ii));
					par.setYPos(MotionCalculator.rec.getYPos(MotionCalculator.groupCounter-clipIndex,ii));
					ii++;
					
				}				
				clipIndex++;
				centerPanel2.repaint();
				
				}});
		
		bottomPanel1.add(backwards, BorderLayout.PAGE_START);
		
		
		
		error2= new JLabel(messages.getString("error2"));
		forward = new JButton (messages.getString("forward"));
		forward.addActionListener(	new ActionListener(){
			
			public void	 actionPerformed(ActionEvent arg0) {
				
				int ii=0;
				if(clipIndex<2) {
					JOptionPane.showMessageDialog(null, error2);
					clipIndex++;
				}
				clipIndex--;
				for(Particle par : centerPanel2.particles) {
					par.setXPos(MotionCalculator.rec.getXPos(MotionCalculator.groupCounter-clipIndex,ii));
					par.setYPos(MotionCalculator.rec.getYPos(MotionCalculator.groupCounter-clipIndex,ii));
					ii++;
					
				}
				
				centerPanel2.repaint();
				
				
				}});
		
		bottomPanel1.add(forward, BorderLayout.PAGE_START);
		
		//BottomPanel2
		
		final int SLIDER_MIN = 0;
		final int SLIDER_MAX = 50;
		final int SLIDER_INIT = 0;
		
		sliderV = new JSlider(JSlider.HORIZONTAL, SLIDER_MIN, SLIDER_MAX, SLIDER_INIT);
		
		
		
		fieldV=new JTextField("0");
		fieldV.addActionListener(new TxtFieldListener(sliderV,fieldV));
		sliderV.addChangeListener(new SliderListener(sliderV,fieldV));
		bottomPanel2Ltop.add(sliderV);
		bottomPanel2Ltop.add(fieldV);
		bottomPanel2L.add(bottomPanel2Ltop);
		
		sliderR = new JSlider(JSlider.HORIZONTAL, SLIDER_MIN, SLIDER_MAX, SLIDER_INIT);
		fieldR=new JTextField("0");
		fieldR.setSize(20,20 );
		sliderR.addChangeListener(new SliderListener(sliderR,fieldR));
		fieldR.addActionListener(new TxtFieldListener(sliderR,fieldR));
		bottomPanel2Rtop.add(sliderR);
		bottomPanel2Rtop.add(fieldR);
		bottomPanel2R.add(bottomPanel2Rtop);
		
		//BottomPanel3
		
		labelV = new JLabel(messages.getString("labelV"));
		bottomPanel2L.add(labelV); 
		
		
		labelR = new JLabel(messages.getString("labelR"));
		bottomPanel2R.add(labelR);
		
		bottomPanel2.add(bottomPanel2L);
		bottomPanel2.add(bottomPanel2R);
	
		
		
  
		
		
		
		
		
		mainPanel.add(centerPanel1, BorderLayout.LINE_START);
		mainPanel.add(centerPanel2, BorderLayout.CENTER);
		mainPanel.add(centerPanel3, BorderLayout.LINE_END);
		centerPanel3.add(centerPanel3l);
		centerPanel3.add(centerPanel3p);
	    bottomPanel.add(bottomPanel1, BorderLayout.PAGE_START);	
		bottomPanel.add(bottomPanel2, BorderLayout.CENTER);	
		bottomPanel.add(bottomPanel3, BorderLayout.PAGE_END);	
		mainPanel.add(bottomPanel, BorderLayout.PAGE_END);
		this.add(mainPanel);
		
		
		
	}
	
	
	
	
	
	private JPanel mainPanel;
	private JPanel centerPanel1;
	BoardPanel centerPanel2;
	private JPanel centerPanel3;
	private JPanel centerPanel3l;
	private JPanel centerPanel3p;
	private JPanel bottomPanel;
	private JPanel bottomPanel1;
	private JPanel bottomPanel2;
	private JPanel bottomPanel2L;
	private JPanel bottomPanel2Ltop;
	private JPanel bottomPanel2R;
	private JPanel bottomPanel2Rtop;
	private JPanel bottomPanel3;
	
	
	BoardButton board1;
	BoardButton board2;
	JButton play;
	JButton stop;
	JButton backwards;
	JButton forward;
	JSlider sliderV;
	JSlider sliderR;
	JSlider sliderL;
	JSlider sliderA;
	JTextField fieldV;
	JTextField fieldR;
	JTextField fieldN;
	JTextField fieldA;
	JLabel labelV;
	JLabel labelR;
	JLabel labelN;
	JLabel labelA;
	JMenu plik;
	JMenu submenu;
	JMenuItem submenu2;
	JMenuItem menuItem;
	JMenuItem menuItem2;
	JMenuItem exit;
	JMenuBar menuBar;
	int clipIndex=2;
	JLabel error1;
	JLabel error2;
	
	
	public static void main(String[] args) {
		MainFrame frame=new MainFrame();
		frame.setVisible(true);

	}


}


