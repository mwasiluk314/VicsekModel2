//author: Mateusz Wasiluk
package pl.edu.pw.fizyka.pojava.AZMW;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;


public class MotionCalculator implements Runnable {
	
	public MotionCalculator(MainFrame frame){
		workspace = frame;
		board = frame.centerPanel2;
		particles = board.particles;
		
	}

	@Override
	public void run() {
		Random rand = new Random();
		rec = new Recorder();
		double averTheta = 0.; //averTheta - �redni k�t wektora pr�dko�ci cia� w promieniu s�siedztwa
		int neighbourCounter = 0;
		
		int saveXPos = 0;
		int saveYPos = 0;
		double centerDistance=0;
		double upCenterDistance=0;
		double downCenterDistance=0;
		Boolean checked = false;
		noise = workspace.sliderA.getValue()*0.01;
		velocity = workspace.sliderV.getValue();
		neighbourRadius = workspace.sliderR.getValue();
		while(runThread == true){
			System.out.print("");//bez tego nie dzia�a
			while(isWorking == true){
				noise = Integer.parseInt(workspace.fieldA.getText());
				velocity = Integer.parseInt(workspace.fieldV.getText());
				neighbourRadius = Integer.parseInt(workspace.fieldR.getText());
				
				rec.addGroup();
				for(Particle par : particles){
					for(Particle checkPar : particles){
						if(Math.abs(par.getXPos()-checkPar.getXPos())<=neighbourRadius && Math.abs(par.getYPos()-checkPar.getYPos())<=neighbourRadius){
							averTheta = averTheta+checkPar.getTheta();
							neighbourCounter++;
						}
					}
					if(neighbourCounter>1){
						averTheta = averTheta - par.getTheta();//Rekompensata iterowania po ca�ej li�cie, w��cznie z obiektem par
						averTheta = averTheta/(double)neighbourCounter;
						par.setDirection(averTheta+noise*(-Math.PI+2*Math.PI*rand.nextDouble()));
						averTheta=0;
						neighbourCounter = 0;
					}
					
				}
				
				
				
				for(Particle par : particles){
					saveXPos = par.getXPos();
					saveYPos = par.getYPos();
					par.moveParticle(velocity);
					if(board.isSecondBoard()==false){
						while(checked==false){
							centerDistance = Math.hypot(par.getXPos()-((double)(board.getBoardXPos()+board.getBoardRInt()/2)), (double)(par.getYPos()-(board.getBoardYPos()+board.getBoardRInt()/2)));
							if(centerDistance>=board.getBoardRExt()/2 || centerDistance<=board.getBoardRInt()/2) {
								par.setXPos(saveXPos);
								par.setYPos(saveYPos);
								par.setDirection(rand.nextInt(359));
								
								par.moveParticle(velocity);
							}
							if(centerDistance<board.getBoardRExt()/2 && centerDistance>board.getBoardRInt()/2){
								checked = true;
							}
							
						}
					}
					
					else while(checked==false){
						centerDistance = Math.hypot(par.getXPos()-((double)(board.getBoardXPos()+board.getBoardRInt()/2)), (double)(par.getYPos()-(board.getBoardYPos()+board.getBoardRInt()/2)));
						upCenterDistance = Math.hypot(par.getXPos()-(board.getBoardXPos()+(board.getBoardRInt2()/4)+board.getBoardRInt2()/2), par.getYPos()-board.getBoardYPos());
						downCenterDistance = Math.hypot(par.getXPos()-(board.getBoardXPos()+(board.getBoardRInt2()/4)+board.getBoardRInt2()/2), par.getYPos()-board.getBoardYPos()-1.5*board.getBoardRInt2());
						
						if(centerDistance>=board.getBoardRExt()/2 || upCenterDistance<=board.getBoardRInt2()/2 || downCenterDistance<=board.getBoardRInt2()/2) {
							par.setXPos(saveXPos);
							par.setYPos(saveYPos);
							par.setDirection(rand.nextInt(359));
							
							par.moveParticle(velocity);
						}
						else checked = true;
						
					}
					checked = false;
					rec.addXPos(groupCounter, par.getXPos());
					rec.addYPos(groupCounter, par.getYPos());
					//rec.getGroup(groupCounter).add(par);
				}
				
				board.repaint();
				groupCounter++;
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
		
	}

	public List<Particle> getParticles(){
		return particles;
	}
	
	public int getNeighbourR(){
		return neighbourRadius;
	}
	
	public double getNoise(){
		return noise;
	}
	
	public int getVelocity(){
		return velocity;
	}
	
	public BoardPanel getBoard(){
		return board;
	}
	
	public void setParticles(List<Particle> particles){
		this.particles = particles;
	}
	
	public void setNeighbourR(int R){
		neighbourRadius = R;
	}
	
	public void setNoise(double n){
		noise = n;
	}
	
	public void setVelocity(int v){
		velocity = v;
	}
	
	public void setBoard(BoardPanel board){
		this.board = board;
	}
	
	public void turnOff(){
		isWorking = false;
	}
	

	
	private List<Particle> particles = new ArrayList<Particle>();
	static int neighbourRadius;
	static double noise;
	static int velocity;
	private Boolean runThread = true;
	static Boolean isWorking = false;
	private BoardPanel board;
	private MainFrame workspace;
	static int groupCounter = 0;
	static Recorder rec;
}
