//author: Anita Zagrobelna
package pl.edu.pw.fizyka.pojava.AZMW;

import javax.swing.SwingUtilities;



public class MainClass {

	public static void main(String[] args) {

		RunnableFrame userFrame = new RunnableFrame();

		SwingUtilities.invokeLater(userFrame);
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		};
		MotionCalculator movement = new MotionCalculator(userFrame.getMainFrame());
		Thread mvmThread = new Thread(movement);
		
		mvmThread.start();
		
	
		
	}

}
