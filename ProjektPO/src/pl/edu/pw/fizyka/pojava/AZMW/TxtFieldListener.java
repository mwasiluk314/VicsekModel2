//author: Mateusz Wasiluk
package pl.edu.pw.fizyka.pojava.AZMW;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JSlider;
import javax.swing.JTextField;

public class TxtFieldListener implements ActionListener {
	
	TxtFieldListener(JSlider slider,JTextField txtField){
		this.slider = slider;
		this.txtField = txtField;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String tmp = new String(txtField.getText());
		Integer val = Integer.parseInt(tmp);
		slider.setValue(val);

	}

	JSlider slider;
	JTextField txtField;
}
