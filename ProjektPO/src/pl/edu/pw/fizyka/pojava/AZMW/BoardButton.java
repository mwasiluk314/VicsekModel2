//author: Mateusz Wasiluk
package pl.edu.pw.fizyka.pojava.AZMW;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JButton;

import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;
import javax.imageio.ImageIO;



public class BoardButton extends JButton {

	   BoardPanel board;
	  BufferedImage image;
	  Boolean secondBoard;
	    BoardButton(String filename, BoardPanel board,Boolean isSecondBoard) {
	            super();
	            this.board = board;
	            try{
	        		image = ImageIO.read(getClass().getResourceAsStream("/Pictures/"+filename));
	        	}
	        	catch(IOException e){
	        		System.err.println("Blad odczytu obrazka");
	        		e.printStackTrace();
	        	}
	            this.secondBoard=isSecondBoard;
	         
	            this.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
					//	board.setImage(filename);
					board.setSecondBoard(secondBoard);
					board.repaint();
						
					}
				} );
	            
	        }

	    protected void paintComponent(Graphics g) {
    		Graphics2D g2d = (Graphics2D)g;
    		g2d.drawImage(image,0,0,100,100,this);
    		
    	}
	     
	     
}
